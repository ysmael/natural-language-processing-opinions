#lecture dun fichier et stockage des lines du fichier dans un tableau
def file_read(rfile):
    with open(rfile) as f:
        tabSW = f.read().splitlines()
    return tabSW

#on stocke les dataset dans un tableau
tabDataSet = file_read('testRmstopW.csv')
print(tabDataSet)

#on stocke les stopWord dans un tableau
tabSW = file_read('stopWords.txt')
print(tabSW)

#fonction qui supprime les stopWord
def RMSW(tabData, tabSW):
    data = tabData
    stopW = tabSW
    out = []
    for line in data:
        for word in stopW:
            tab = line.replace(word,"")
            out.append(tab)
    return out

dataWSW = RMSW(tabDataSet,tabSW)
print(dataWSW)
